# MobSF analyzer changelog

## v2.5.0
- Update MobSF to version 3.2.9 (!15)
  + Includes OWASP MSTG mappings in results: https://github.com/MobSF/Mobile-Security-Framework-MobSF/commit/e5107dc5918a76a0b8fa2c800475ad093746067c
  + Adds Support for Android 10 API 29 for Genymotion: https://github.com/MobSF/Mobile-Security-Framework-MobSF/commit/000dc362ab5bfc9d0d4d32a61dd093fb6e377be5
  + Fixes a template bug in the File Analysis Section of Android Source: https://github.com/MobSF/Mobile-Security-Framework-MobSF/pull/1640
  + xapk support https://github.com/MobSF/Mobile-Security-Framework-MobSF/commit/2648bf918a206066a33b891152fa36eb9c3acb21
  + Updates apktool to 2.5.0 https://github.com/MobSF/Mobile-Security-Framework-MobSF/commit/2417f1423f32d2ac3885f4b8ab7abb2688354e81
  + Allows insecure regex matches with kotlin https://github.com/MobSF/Mobile-Security-Framework-MobSF/commit/eef603bc4009b88dd183f2ddaed009c112c730c0
  + NIAP Analysis for Android: https://github.com/MobSF/Mobile-Security-Framework-MobSF/commit/d1f8f87d2d2a21c72223b7b39288a127c924d61f
  + Secret Detection: https://github.com/MobSF/Mobile-Security-Framework-MobSF/commit/81bd0aabda6f3a865ef30a263a959002745616f7

## v2.4.0
- Add Identifiers to Issue (!12)

## v2.3.0
- Update common to v2.22.0 (!11)
- Update urfave/cli to v2.3.0 (!11)

## v2.2.0
- Enable ruleset disablement (!9)

## v2.1.0
- Map severity `secure` to `severityLevel.Info` (!8)

## v2.0.0
- Finalize v2.0.0 release (!3)
- Mock project into MobSFs expected structure (!1)

## v1.0.0
- Initial beta release. Code provided by H-E-B (!2)
