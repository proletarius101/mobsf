FROM golang:1.15 as build

WORKDIR /go/src/app
COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
  PATH_TO_MODULE=`go list -m` && \
  go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o /analyzer

FROM debian:stable-slim

ARG SCANNER_VERSION

# SCANNER_VERSION specified here is just for report generation purpose. 
# We must ensure that this version matches the version mentioned the template: https://gitlab.com/gitlab-org/gitlab/-/blob/29cc646679b927158b3315dbc6c5af39524fd6ec/lib/gitlab/ci/templates/Security/SAST.gitlab-ci.yml
ENV SCANNER_VERSION ${SCANNER_VERSION:-3.2.9}

COPY --from=build /analyzer /analyzer

CMD ["/analyzer", "run"]
